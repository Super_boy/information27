# -*- coding:utf-8 -*-
# 登录注册的相关逻辑都放在当前模块
from flask import Blueprint

# 创建蓝图对象
passport_blu = Blueprint('passport_blu', __name__, url_prefix="/passport")

from . import views