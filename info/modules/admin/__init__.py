from flask import Blueprint

# 创建蓝图对象
from flask import redirect
from flask import request
from flask import session
from flask import url_for

admin_blu = Blueprint('admin', __name__, url_prefix="/admin")

from . import views

@admin_blu.before_request
def check_admin():
    # if 不是管理员,那么直接跳转到后台主页
    is_admin = session.get("is_admin", False)

    # if not is_admin and 当前访问的url不是管理登录页:
    if not is_admin and not request.url.endswith(url_for('admin.login')):
        # 判断当前是否有用户登录，或者是否是管理员，如果不是，直接重定向到项目主页
        return redirect("/")
